<?php

namespace Swigle\Lucretia\Mage2\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Class InstallData
 *
 * @package Swigle\Lucretia\Mage2\Helper
 * @author Delano de Rooij <delano@swigle.com>
 * @since 22/11/2017
 *
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * @inheritdoc
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @void
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)//: void
    {
        /**
         * Creates the Profit order related statuses
         */
        $statuses = [];
        $statuses[] = ['status' => 'pushed_to_profit', 'label' => __('in Profit')];
        $statuses[] = ['status' => 'bounced_by_profit', 'label' => __('Bounced by Profit')];
        $setup->getConnection()->insertArray(
            $setup->getTable('sales_order_status'),
            ['status', 'label'],
            $statuses
        );

        /**
         * Map statuses to states
         */
        $stateStatusMapping = [];
        foreach (['new', 'processing'] as $state) {
            foreach ($statuses as $status) {
                $stateStatusMapping[] = [
                    'status' => $status['status'],
                    'state' => $state,
                    'is_default' => 0,
                    'visible_on_front' => 0
                ];
            }
        }

        $setup->getConnection()->insertArray(
            $setup->getTable('sales_order_status_state'),
            ['status', 'state', 'is_default', 'visible_on_front'],
            $stateStatusMapping
        );
    }
}
