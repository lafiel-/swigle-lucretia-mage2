<?php

namespace Swigle\Lucretia\Mage2\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class ProfitFbSalesConfig
 *
 * @package Swigle\Lucretia\Mage2\Helper
 * @author Delano de Rooij <delano@swigle.com>
 * @since 22/11/2017
 */
class ProfitFbSalesConfig
{
    /**
     * @var ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * Default value for "magazijn" (fbsales -> war)
     * @return string
     */
    public function getWar(): string
    {
        return $this->_scopeConfig->getValue('swigle_lucretia/fbsales/war', ScopeInterface::SCOPE_WEBSITE);
    }
}
