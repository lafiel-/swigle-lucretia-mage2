<?php

namespace Swigle\Lucretia\Mage2\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class ProfitKnSalesRelationConfig
 *
 * @package Swigle\Lucretia\Mage2\Helper
 * @author Delano de Rooij <delano@swigle.com>
 * @since 22/11/2017
 */
class ProfitKnSalesRelationConfig
{
    /**
     * @var ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * Default value for "verzamelrekening" (knsalesrelation -> ColA)
     * @return string
     */
    public function getColA(): string
    {
        return $this->_scopeConfig->getValue('swigle_lucretia/knsalesrelation/cola', ScopeInterface::SCOPE_WEBSITE);
    }

    /**
     * Default value for "betalingsvoorwaarde" (knsalesrelation -> PaCd)
     * @return string
     */
    public function getPaCd(): string
    {
        return $this->_scopeConfig->getValue('swigle_lucretia/knsalesrelation/pacd', ScopeInterface::SCOPE_WEBSITE);
    }

    /**
     * Default value for "btw-plicht" (knsalesrelation -> VaDu)
     * @return string
     */
    public function getVaDu(): string
    {
        return $this->_scopeConfig->getValue('swigle_lucretia/knsalesrelation/vadu', ScopeInterface::SCOPE_WEBSITE);
    }

    /**
     * Default value for "leveringsconditie" (knsalesrelation -> DeCo)
     * @return string
     */
    public function getDeCo(): string
    {
        return $this->_scopeConfig->getValue('swigle_lucretia/knsalesrelation/deco', ScopeInterface::SCOPE_WEBSITE);
    }
}
