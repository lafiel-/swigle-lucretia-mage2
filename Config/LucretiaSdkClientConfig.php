<?php

namespace Swigle\Lucretia\Mage2\Config;

use \Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class LucretiaSdkClientConfig
 *
 * @package Swigle\Lucretia\Mage2\Config
 * @author Delano de Rooij <delano@swigle.com>
 * @since 22/11/2017
 */
class LucretiaSdkClientConfig
{
    /**
     * @var string
     */
    public $endpoint;

    /**
     * @var string
     */
    public $token;

    /**
     * @var string
     */
    public $company;

    /**
     * @var string
     */
    public $environment;

    /**
     * LucretiaSdkClientConfig constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->endpoint = $scopeConfig->getValue('swigle_lucretia/api/lucretia_api_endpoint',       \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE);
        $this->token = $scopeConfig->getValue('swigle_lucretia/api/lucretia_api_token',             \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE);
        $this->company = $scopeConfig->getValue('swigle_lucretia/api/lucretia_api_company',         \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE);
        $this->environment = $scopeConfig->getValue('swigle_lucretia/api/lucretia_api_environment', \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE);
    }
}