<?php

namespace Swigle\Lucretia\Mage2\SDK;

use GuzzleHttp\ClientInterface as HttpClientInterface;
use Swigle\Osiris\SDK\ResultFactory;

/**
 * Class Client
 *
 * @package Swigle\Lucretia\Mage2\Helper
 * @author Delano de Rooij <delano@swigle.com>
 * @since 22/11/2017
 */
class Client extends \Swigle\Osiris\SDK\Client
{
    /**
     * Client constructor.
     * @param \Swigle\Lucretia\Mage2\Config\LucretiaSdkClientConfig $config
     * @param HttpClientInterface|null $client
     * @param ResultFactory|null $resultFactory
     */
    public function __construct(\Swigle\Lucretia\Mage2\Config\LucretiaSdkClientConfig $config, HttpClientInterface $client = null, ResultFactory $resultFactory = null)
    {
        parent::__construct((array)$config, $client, $resultFactory);
    }
}
