<?php

namespace Swigle\Lucretia\Mage2\Console\Command;

use \Magento\Framework\App\State;
use \Swigle\Lucretia\Mage2\SDK\Client;

/**
 * Class LucretiaCommand
 *
 * @package Swigle\Lucretia\Mage2\Helper
 * @author Delano de Rooij <delano@swigle.com>
 * @since 22/11/2017
 */
class LucretiaCommand extends \Symfony\Component\Console\Command\Command
{
    /**
     * @var \Swigle\Lucretia\Mage2\SDK\Client $lucretiaClient
     */
    protected $_lucretiaClient;

    /**
     * LucretiaCommand constructor.
     * @param State $state
     * @param Client $lucretiaClient
     */
    public function __construct(State $state, Client $lucretiaClient)
    {
        // Attempt to set CLI-proces area code for core methods
        try {
            $state->setAreaCode('adminhtml');
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            // @todo area code exception handling, perhaps some logging?!
        }

        // Register at parent
        parent::__construct();

        // Initialise Lucretia client
        $this->_lucretiaClient = $lucretiaClient;
    }
}
