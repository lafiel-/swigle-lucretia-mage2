<?php

namespace Swigle\Lucretia\Mage2\Console\Command;

use Exception;
use Magento\Sales\Model\OrderRepositoryFactory;
use Magento\Sales\Model\Order\Status\HistoryFactory;
use Magento\Sales\Model\Order\Status\HistoryRepositoryFactory;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\Framework\App\State;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Phrase;
use Swigle\Lucretia\Mage2\Config\ProfitFbSalesConfig;
use Swigle\Lucretia\Mage2\Config\ProfitKnSalesRelationConfig;
use Swigle\Lucretia\Mage2\SDK\Client as SDKClient;
use Symfony\Component\Console\Input\InputInterface as ConsoleInputInterface;
use Symfony\Component\Console\Output\OutputInterface as ConsoleOutputInterface;

/**
 * Class OrderPushCommand
 *
 * @package Swigle\Lucretia\Mage2\Helper
 * @author Delano de Rooij <delano@swigle.com>
 * @since 22/11/2017
 */
class OrderPushCommand extends LucretiaCommand
{
    // Order statuses
    const ORDER_STATUS_PROFIT_PUSHED = 'pushed_to_profit';
    const ORDER_STATUS_PROFIT_BOUNCED = 'bounced_by_profit';

    /**
     * @var \Magento\Sales\Model\OrderRepository
     */
    private $_orderRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $_searchCriteriaBuilder;

    /**
     * @var \Magento\Sales\Model\Order\Status\HistoryRepository
     */
    private $_orderHistoryRepository;

    /**
     * @var HistoryFactory
     */
    private $_orderHistoryFactory;

    /**
     * @var ProfitFbSalesConfig
     */
    private $_fbSalesConfig;

    /**
     * @var ProfitKnSalesRelationConfig
     */
    private $_knSalesRelationConfig;

    /**
     * @param State $state
     * @param SDKClient $lucretiaClient
     * @param OrderRepositoryFactory $orderRepositoryFactory
     * @param SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory
     * @param HistoryRepositoryFactory $historyRepositoryFactory
     * @param HistoryFactory $historyFactory
     * @param ProfitFbSalesConfig $profitFbSalesConfig
     * @param ProfitKnSalesRelationConfig $profitKnSalesRelationConfig
     */
    public function __construct(
        State $state,
        SDKClient $lucretiaClient,
        OrderRepositoryFactory $orderRepositoryFactory,
        SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory,
        HistoryRepositoryFactory $historyRepositoryFactory,
        HistoryFactory $historyFactory,
        ProfitFbSalesConfig $profitFbSalesConfig,
        ProfitKnSalesRelationConfig $profitKnSalesRelationConfig
    ) {
        parent::__construct($state, $lucretiaClient);

        $this->_orderRepository        = $orderRepositoryFactory->create();
        $this->_searchCriteriaBuilder  = $searchCriteriaBuilderFactory->create();
        $this->_orderHistoryRepository = $historyRepositoryFactory->create();
        $this->_orderHistoryFactory    = $historyFactory;
        $this->_fbSalesConfig          = $profitFbSalesConfig;
        $this->_knSalesRelationConfig  = $profitKnSalesRelationConfig;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()//: void
    {
        $this->setName('lucretia:order:push');
        $this->setDescription('Pushes all orders that are being processed in Magento to Profit');

        parent::configure();
    }

    /**
     * @inheritdoc
     * @param ConsoleInputInterface $input
     * @param ConsoleOutputInterface $output
     * @return int|null
     * @throws CouldNotSaveException
     */
    protected function execute(ConsoleInputInterface $input, ConsoleOutputInterface $output): int
    {
        $output->writeln('Started ' . $this->getName() . ' at ' . date("H:i:s"));

        $this->_searchCriteriaBuilder->addFilter('state', ['processing'], 'in');
        $this->_searchCriteriaBuilder->addFilter('status', [self::ORDER_STATUS_PROFIT_PUSHED, self::ORDER_STATUS_PROFIT_BOUNCED], 'nin');
        $searchCriteria = $this->_searchCriteriaBuilder->create();

        $orders = $this->_orderRepository->getList($searchCriteria);
        if ($orders->getTotalCount() >= 1) {
            foreach ($orders->getItems() as $order) {
                $statusHistoryComment = $this->_orderHistoryFactory->create();
                $statusHistoryComment->setOrder($order);
                $statusHistoryComment->setIsCustomerNotified(false);
                $statusHistoryComment->setIsVisibleOnFront(false);

                try {
                    $debtor_id = $this->fetchDebtorIdentifier($order);
                    $address_id = $this->fetchAddressIdentifier($debtor_id, $order);
                    $order_number = $this->pushOrder($order, $debtor_id, $address_id);
                    $order->setStatus(self::ORDER_STATUS_PROFIT_PUSHED);
                    $statusHistoryComment->setStatus(self::ORDER_STATUS_PROFIT_PUSHED);
                    $statusHistoryComment->setComment('Profit ordernummer: ' . $order_number);
                } catch (CouldNotSaveException $e) {
                    $order->setStatus(self::ORDER_STATUS_PROFIT_BOUNCED);
                    $statusHistoryComment->setStatus(self::ORDER_STATUS_PROFIT_BOUNCED);
                    $statusHistoryComment->setComment($e->getMessage());
                } catch (\GuzzleHttp\Exception\ServerException $e) {
                    $order->setStatus(self::ORDER_STATUS_PROFIT_BOUNCED);
                    $statusHistoryComment->setStatus(self::ORDER_STATUS_PROFIT_BOUNCED);
                    $exceptionContent = json_decode($e->getResponse()->getBody()->getContents());
                    $comment = (is_object($exceptionContent) && isset($exceptionContent->profitLogReference) && isset($exceptionContent->externalMessage)) ? "Profit log reference: " . $exceptionContent->profitLogReference . "<br>" . $exceptionContent->externalMessage : "Profit bounce!";
                    $statusHistoryComment->setComment($comment);
                }

                $this->_orderRepository->save($order);
                $this->_orderHistoryRepository->save($statusHistoryComment);
            }
        }

        $output->writeln('Process ' . $this->getName() . ' finished at ' . date("H:i:s"));

        return 0;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return int
     * @throws \Exception
     */
    private function fetchDebtorIdentifier(\Magento\Sales\Model\Order $order): int
    {
        try {
            $debtor_id = $this->searchForDebtorByEmail($order->getCustomerEmail());
        } catch (NoSuchEntityException $e) {
            $debtor_id = $this->registerDebtor($order);
        }
        return $debtor_id;
    }

    /**
     * @param string $email
     * @return int
     * @throws NoSuchEntityException
     */
    private function searchForDebtorByEmail(string $email): int
    {
        $response = $this->_lucretiaClient->passthrough('get', 'connectors/Profit_Debtor', ['filterfieldids' => 'Email', 'filtervalues' => $email], []);
        if (isset($response->rows[0]->DebtorId)) {
            return $response->rows[0]->DebtorId;
        } else {
            throw new NoSuchEntityException();
        }
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return int
     * @throws \Exception
     */
    private function registerDebtor(\Magento\Sales\Model\Order $order): int
    {
        $knSalesRelationRequest = [
            'KnSalesRelationPer' => [
                'Element' => [
                    'Fields' => [
                        'IsDb' => 1,
                        'InPv' => 'E',
                        'CuId' => 'EUR',
                        'ColA' => $this->_knSalesRelationConfig->getColA(),
                        'PaCd' => $this->_knSalesRelationConfig->getPaCd(),
                        'VaDu' => $this->_knSalesRelationConfig->getVaDu(),
                        'DeCo' => $this->_knSalesRelationConfig->getDeCo()
                    ],
                    'Objects' => [
                        'KnPerson' => [
                            'Element' => [
                                'Fields' => [
                                    'PadAdr' => 1,
                                    'AutoNum' => 1,
                                    'MatchPer' => 7,
                                    'ViGe' => 'O',
                                    'FiNm' => $order->getShippingAddress()->getFirstname(),
                                    'LaNm' => $order->getShippingAddress()->getLastname(),
                                    'TeNr' => $order->getShippingAddress()->getTelephone(),
                                    'EmAd' => $order->getCustomerEmail()
                                ],
                                'Objects' => [
                                    'KnBasicAddressAdr' => [
                                        'Element' => [
                                            'Fields' => [
                                                'PbAd' => 0,
                                                'ResZip' => false,
                                                'CoId' => 'NL',
                                                'Ad' => $order->getShippingAddress()->getStreetLine(1),
                                                'HmNr' => $order->getShippingAddress()->getStreetLine(2),
                                                'HmAd' => $order->getShippingAddress()->getStreetLine(3),
                                                'ZpCd' => $order->getShippingAddress()->getPostcode(),
                                                'Rs' => $order->getShippingAddress()->getCity()
                                            ]
                                        ]
                                    ],
                                    'KnContact' => [
                                        'Element' => [
                                            'Fields' => [
                                                'Bl' => 0,
                                                'ViKc' => 'AFL',
                                                'PadAdr' => 1
                                            ],
                                            'Objects' => [
                                                'KnBasicAddressAdr' => [
                                                    'Element' => [
                                                        'Fields' => [
                                                            'PbAd' => 0,
                                                            'ResZip' => false,
                                                            'CoId' => 'NL',
                                                            'Ad' => $order->getShippingAddress()->getStreetLine(1),
                                                            'HmNr' => $order->getShippingAddress()->getStreetLine(2),
                                                            'HmAd' => $order->getShippingAddress()->getStreetLine(3),
                                                            'ZpCd' => $order->getShippingAddress()->getPostcode(),
                                                            'Rs' => $order->getShippingAddress()->getCity()
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                        ],
                    ]
                ]
            ]
        ];

        try {
            $knSalesRelationRequest = $this->alterKnSalesRelationPerBeforePush($knSalesRelationRequest, $order);
            $response = $this->_lucretiaClient->passthrough('post', 'connectors/KnSalesRelationPer', [], $knSalesRelationRequest);
            if (isset($response->results->KnSalesRelationPer->DbId)) {
                $debtor_id = $response->results->KnSalesRelationPer->DbId;
                $this->afterKnSalesRelationPush($debtor_id, $order);
                return $debtor_id;
            }

            throw new Exception;

        } catch (Exception $e) {
            throw new CouldNotSaveException(new Phrase('Aanmaken van de verkooprelatie is mislukt'));
        }
    }

    /**
     * @param int $debtor_id
     * @param \Magento\Sales\Model\Order $order
     * @param int $registerAttempt
     * @throws \Exception | CouldNotSaveException
     * @return int
     */
    private function fetchAddressIdentifier(int $debtor_id, \Magento\Sales\Model\Order $order, int $registerAttempt = 0): int
    {
        try {
            $response = $this->_lucretiaClient->passthrough('get', 'connectors/Profit_Deliveryaddress', [
                'filterfieldids' => 'DebiteurID',
                'filtervalues' => $debtor_id
            ], []);

            if (is_object($response) && isset($response->rows) && !empty($response->rows)) {
                foreach ($response->rows as $row) {
                    if (
                        (strtolower(str_replace(" ", "", $row->{'A_Postcode'})) == strtolower(str_replace(" ", "", $order->getShippingAddress()->getPostcode()))) &&
                        ($row->{'A_Huisnummer'} == $order->getShippingAddress()->getStreetLine(2)) &&
                        ($row->{'A_Toev_Huisnr.'} == (string)$order->getShippingAddress()->getStreetLine(3))
                    ) {
                        return $row->Adres_ID;
                    }
                }
            }

            throw new Exception("No delivery address found");
        } catch (Exception $e) {
            if ($registerAttempt < 2) {
                return $this->registerAddressForDebtor($debtor_id, $order, $registerAttempt);
            }

            throw new CouldNotSaveException(new Phrase('Aanmaken van het afleveradres voor debiteur is mislukt'));
        }
    }

    /**
     * @param int $debtor_id
     * @param \Magento\Sales\Model\Order $order
     * @param int $registerAttempt
     * @throws CouldNotSaveException
     * @return int
     */
    private function registerAddressForDebtor(int $debtor_id, \Magento\Sales\Model\Order $order, int $registerAttempt = 0): int
    {
        $knSalesRelationRequest = [
            'KnSalesRelationPer' => [
                'Element' => [
                    '@DbId' => $debtor_id,
                    'Objects' => [
                        'KnPerson' => [
                            'Element' => [
                                'Fields' => [
                                    'MatchPer' => 9
                                ],
                                'Objects' => [
                                    'KnContact' => [
                                        'Element' => [
                                            'Fields' => [
                                                'Bl' => 0,
                                                'ViKc' => 'AFL',
                                                'PadAdr' => 1
                                            ],
                                            'Objects' => [
                                                'KnBasicAddressAdr' => [
                                                    'Element' => [
                                                        'Fields' => [
                                                            'PbAd' => 0,
                                                            'ResZip' => false,
                                                            'CoId' => 'NL',
                                                            'Ad' => $order->getShippingAddress()->getStreetLine(1),
                                                            'HmNr' => $order->getShippingAddress()->getStreetLine(2),
                                                            'HmAd' => $order->getShippingAddress()->getStreetLine(3),
                                                            'ZpCd' => $order->getShippingAddress()->getPostcode(),
                                                            'Rs' => $order->getShippingAddress()->getCity()
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                        ]
                    ]
                ]
            ]
        ];

        $knSalesRelationRequest = $this->alterKnSalesRelationPerBeforePush($knSalesRelationRequest, $order);

        try {
            $response = $this->_lucretiaClient->passthrough('put', 'connectors/KnSalesRelationPer', [], $knSalesRelationRequest, $statusCode);
            if ($response === NULL && $statusCode >= 200 && $statusCode < 300) {
                return $this->fetchAddressIdentifier($debtor_id, $order, ++$registerAttempt);
            } else {
                throw new Exception;
            }
        } catch (Exception $e) {
            throw new CouldNotSaveException(new Phrase('Aanmaken van het afleveradres voor debiteur is mislukt'));
        }
    }

    /**
     * @param array $knSalesRelationRequest
     * @param \Magento\Sales\Model\Order $order
     * @return array
     */
    protected function alterKnSalesRelationPerBeforePush(array $knSalesRelationRequest, \Magento\Sales\Model\Order $order): array
    {
        return $knSalesRelationRequest;
    }

    /**
     * @param int $debtor_id
     * @param \Magento\Sales\Model\Order $order
     */
    protected function afterKnSalesRelationPush(int $debtor_id, \Magento\Sales\Model\Order $order)
    {
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @param int $debtor_id
     * @param int $address_id
     * @return int
     * @throws \Exception
     */
    private function pushOrder(\Magento\Sales\Model\Order $order, int $debtor_id, int $address_id)
    {
        $fbSalesRequest = [
            'FbSales' => [
                'Element' => [
                    'Fields' => [
                        'OrDa' => date('Y-m-d', strtotime($order->getCreatedAt())),
                        'DbId' => $debtor_id,
                        'DlAd' => $address_id,
                        'War' => $this->_fbSalesConfig->getWar(),
                        'RfCs' => '#' . $order->getIncrementId()
                    ],
                    'Objects' => [
                        'FbSalesLines' => [
                            'Element' => []
                        ]
                    ]
                ]
            ]
        ];

        foreach ($order->getItems() as $orderItem) {
            $fbSalesRequest['FbSales']['Element']['Objects']['FbSalesLines']['Element'][] = [
                'Fields' => [
                    'VaIt' => 'Art',
                    'ItCd' => $orderItem->getSku(),
                    'QuUn' => $orderItem->getQtyOrdered()
                ]
            ];
        }

        $fbSalesRequest = $this->alterFbSalesBeforePush($fbSalesRequest, $order);
        $response = $this->_lucretiaClient->passthrough('post', 'connectors/FbSales', [], $fbSalesRequest);

        if (isset($response->results->FbSales[0]->OrNu)) {
            $orderNumber = $response->results->FbSales[0]->OrNu;
            $this->afterFbSalesPush($orderNumber, $order);
            return $orderNumber;
        }

        throw new CouldNotSaveException(new Phrase('Aanmaken van de verkooporder is mislukt'));
    }

    /**
     * @param array $fbSalesRequest
     * @param \Magento\Sales\Model\Order $order
     * @return array
     */
    protected function alterFbSalesBeforePush(array $fbSalesRequest, \Magento\Sales\Model\Order $order): array
    {
        return $fbSalesRequest;
    }

    /**
     * @param int $orderNumber
     * @param \Magento\Sales\Model\Order $order
     */
    protected function afterFbSalesPush(int $orderNumber, \Magento\Sales\Model\Order $order)
    {
    }
}
