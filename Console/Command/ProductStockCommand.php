<?php

namespace Swigle\Lucretia\Mage2\Console\Command;

use Exception;
use Magento\CatalogInventory\Model\StockRegistryFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\App\State;
use Swigle\Lucretia\Mage2\SDK\Client as SDKClient;
use Symfony\Component\Console\Input\InputInterface as ConsoleInputInterface;
use Symfony\Component\Console\Output\OutputInterface as ConsoleOutputInterface;

/**
 * Class ProductStockCommand
 *
 * @package Swigle\Lucretia\Mage2\Helper
 * @author Delano de Rooij <delano@swigle.com>
 * @since 22/11/2017
 */
class ProductStockCommand extends LucretiaCommand
{
    /**
     * @var  \Magento\CatalogInventory\Model\StockRegistry
     */
    protected $_stockRegistry;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected $_productCollection;

    /**
     * ProductStockCommand constructor.
     * @param State $state
     * @param SDKClient $lucretiaClient
     * @param StockRegistryFactory $stockRegistryFactory
     * @param ProductCollectionFactory $productCollectionFactory
     */
    public function __construct(
        State $state,
        SDKClient $lucretiaClient,
        StockRegistryFactory $stockRegistryFactory,
        ProductCollectionFactory $productCollectionFactory
    ) {
        parent::__construct($state, $lucretiaClient);
        $this->_stockRegistry = $stockRegistryFactory->create();
        $this->_productCollection = $productCollectionFactory->create();
    }

    /**
     * {@inheritdoc}
     * @void
     */
    protected function configure()//: void
    {
        $this->setName('lucretia:product:stock');
        $this->setDescription('Refreshes the product stock');

        parent::configure();
    }

    /**
     * Loops trough all the stock records that are being found in the Lucretia database and updates the stock positions at the respective products
     * @inheritdoc
     * @param ConsoleInputInterface $input
     * @param ConsoleOutputInterface $output
     * @return int
     * @throws Exception
     */
    public function execute(ConsoleInputInterface $input, ConsoleOutputInterface $output): int
    {
        $output->writeln('Started at ' . date("H:i:s"));

        try {
            // Lucretia product registry
            $lucretiaBatchProducts = [];

            // Lucretia records
            foreach ($this->_lucretiaClient->getProductStocks() as $lucretiaProductStockRecord) {
                if ($this->_registerStock($lucretiaProductStockRecord->Itemcode, $lucretiaProductStockRecord->OpVoorraad)) {
                    $lucretiaBatchProducts[] = $lucretiaProductStockRecord->Itemcode;
                }
            }

            // Set stock to 0 for products which don't appear in the Lucretia connector
            $numberOfPages = $this->_productCollection->setPageSize(250)->getLastPageNumber();
            for ($i = 1; $i <= $numberOfPages; $i++) {
                $this->_productCollection->setCurPage($i)->load();
                foreach ($this->_productCollection->getItems() as $product) if (!in_array($product['sku'], $lucretiaBatchProducts)) $this->_registerStock($product['sku'], 0);
                $this->_productCollection->clear();
            }

        } catch (Exception $e) {
            $output->writeln('Caught generic error! Process halted.');
            throw $e;
        }

        $output->writeln('Process finished at ' . date("H:i:s"));
    }

    /**
     * Register stock for product
     * @param string $productSKU
     * @param int $qty
     * @return bool
     */
    private function _registerStock(string $productSKU, int $qty): bool
    {
        try {
            $stockItem = $this->_stockRegistry->getStockItemBySku($productSKU);
            $stockItem->setQty($qty);
            $stockItem->setIsInStock((bool)$qty);
            $this->_stockRegistry->updateStockItemBySku($productSKU, $stockItem);
            return true;
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return false;
        }
    }
}
