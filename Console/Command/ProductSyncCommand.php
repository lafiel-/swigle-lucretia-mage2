<?php

namespace Swigle\Lucretia\Mage2\Console\Command;

use Magento\Catalog\Model\ProductRepositoryFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\CatalogInventory\Model\StockRegistryFactory;
use Magento\Framework\App\State;
use Swigle\Lucretia\Mage2\Helper\ProductHelper;
use Swigle\Lucretia\Mage2\SDK\Client as SDKClient;
use Symfony\Component\Console\Input\InputInterface as ConsoleInputInterface;
use Symfony\Component\Console\Output\OutputInterface as ConsoleOutputInterface;

/**
 * Class ProductSyncCommand
 *
 * @package Swigle\Lucretia\Mage2\Helper
 * @author Delano de Rooij <delano@swigle.com>
 * @since 22/11/2017
 */
class ProductSyncCommand extends LucretiaCommand
{
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \Swigle\Lucretia\Mage2\Helper\ProductHelper
     */
    protected $_productHelper;

    /**
     * @var \Magento\CatalogInventory\Model\StockRegistry
     */
    protected $_stockRegistry;

    /**
     * ProductsSyncCommand constructor.
     * @param State $state
     * @param SDKClient $lucretiaClient
     * @param ProductRepositoryFactory $productRepositoryFactory
     * @param ProductFactory $productFactory
     * @param ProductHelper $productHelper
     * @param StockRegistryFactory $stockRegistryFactory
     */
    public function __construct(
        State $state,
        SDKClient $lucretiaClient,
        ProductRepositoryFactory $productRepositoryFactory,
        ProductFactory $productFactory,
        ProductHelper $productHelper,
        StockRegistryFactory $stockRegistryFactory
    ) {
        parent::__construct($state, $lucretiaClient);
        $this->_productRepository = $productRepositoryFactory->create();
        $this->_productFactory = $productFactory;
        $this->_productHelper = $productHelper;
        $this->_stockRegistry = $stockRegistryFactory->create();
    }

    /**
     * {@inheritdoc}
     * @void
     */
    protected function configure()//: void
    {
        $this->setName('lucretia:product:sync');
        $this->setDescription('Fetches all products from Lucretia and creates or updates them in Magento');
        parent::configure();
    }

    /**
     * Loops trough all the products that are being found in the Lucretia database and creates or updates them
     * @inheritdoc
     * @param ConsoleInputInterface $input
     * @param ConsoleOutputInterface $output
     * @return int
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     */
    protected function execute(ConsoleInputInterface $input, ConsoleOutputInterface $output): int
    {
        $output->writeln('Started at ' . date("H:i:s"));

        foreach ($this->_lucretiaClient->getProducts() as $lucretiaProductRecord) {
            // Check if all required fields are present and filled
            if (empty($lucretiaProductRecord->Itemcode) || empty($lucretiaProductRecord->Omschrijving) || empty($lucretiaProductRecord->Basisverkoopprijs)) {
                $output->writeln('Skipping Lucretia record ' . $lucretiaProductRecord->lucretia_id . ' because not all the required data is not present.');
                continue;
            }

            // Check if product exists, if not, create entity
            $isNewRecord = false;
            try {
                $product = $this->_productRepository->get($lucretiaProductRecord->Itemcode);
            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                $product = $this->_productFactory->create();
                $isNewRecord = true;
            }

            // New product attributes
            if ($isNewRecord) {
                $product->setSku($lucretiaProductRecord->Itemcode);
                $product->setTypeId(\Magento\Catalog\Model\Product\Type::TYPE_SIMPLE);
                $product->setVisibility(\Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH);
                $product->setAttributeSetId($product->getDefaultAttributeSetId());
                $product->setPrice($lucretiaProductRecord->Basisverkoopprijs);
                $product->setUrlKey($this->_productHelper->generateCleanUrl($lucretiaProductRecord->Omschrijving . '-' . $lucretiaProductRecord->Itemcode));
            }

            // Determine product status
            if (isset($lucretiaProductRecord->Geblokkeerd) && $lucretiaProductRecord->Geblokkeerd == 1) {
                $product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED);
            } else {
                $product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
            }

            // Map attributes that are always being updated
            $product->setName($lucretiaProductRecord->Omschrijving);

            // Possibility to apply vendor specific business rules before the product has been saved
            $product = $this->alterProductBeforeSave($product, $lucretiaProductRecord);

            // Attempt to save product
            try {
                $this->_productRepository->save($product);
            } catch (\Magento\Framework\Exception\CouldNotSaveException $e) {
                $output->writeln('Unable to save product - ' . $product->getSku());
                $output->writeln($e);
                continue;
            }

            // Register stock only for new products
            if ($isNewRecord) {
                $stockItem = $this->_stockRegistry->getStockItem($product->getId());
                $stockItem->setProductId($product->getId());
                $stockItem->setQty($lucretiaProductRecord->BeschikbareVoorraad);
                $stockItem->setIsInStock((bool)$lucretiaProductRecord->BeschikbareVoorraad);
                try {
                    $this->_stockRegistry->updateStockItemBySku($product->getSku(), $stockItem);
                } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                    $output->writeln('Unable to save product (' . $product->getSku() . ') stock');
                    $output->writeln($e);
                    continue;
                }
            }

            // Possibility to apply vendor specific business rules after the product has been saved
            $this->afterProductSave($product, $lucretiaProductRecord);
        }

        $output->writeln('Process finished at ' . date("H:i:s"));

        return 0;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param $lucretiaProductRecord
     * @return \Magento\Catalog\Model\Product
     */
    protected function alterProductBeforeSave(\Magento\Catalog\Model\Product $product, $lucretiaProductRecord): \Magento\Catalog\Model\Product
    {
        return $product;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param $lucretiaProductRecord
     */
    protected function afterProductSave(\Magento\Catalog\Model\Product $product, $lucretiaProductRecord)
    {
    }
}
