<?php

namespace Swigle\Lucretia\Mage2\Console\Command;

use Magento\Catalog\Model\ProductRepositoryFactory;
use Magento\Catalog\Api\ProductAttributeMediaGalleryManagementInterface;
use Magento\Catalog\Model\Product\Gallery\EntryFactory;
use Magento\Framework\Api\ImageContentFactory;
use Magento\Framework\App\State;
use Swigle\Lucretia\Mage2\Helper\ProductHelper;
use Swigle\Lucretia\Mage2\SDK\Client as SDKClient;
use Symfony\Component\Console\Input\InputInterface as ConsoleInputInterface;
use Symfony\Component\Console\Output\OutputInterface as ConsoleOutputInterface;

/**
 * Class ProductImagesCommand
 *
 * @package Swigle\Lucretia\Mage2\Console\Command
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 14/11/2018 14:19
 */
class ProductImagesCommand extends LucretiaCommand
{
    /**
     * @var  \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var  \Swigle\Lucretia\Mage2\Helper\ProductHelper
     */
    protected $_productHelper;

    /**
     * @var  \Magento\Catalog\Api\ProductAttributeMediaGalleryManagementInterface
     */
    protected $_productAttributeMediaGalleryManagement;

    /**
     * @var  \Magento\Catalog\Model\Product\Gallery\EntryFactory
     */
    protected $_productGalleryEntryFactory;

    /**
     * @var  \Magento\Framework\Api\ImageContentFactory
     */
    protected $_imageContentFactory;

    /**
     * ProductImagesCommand constructor.
     * @param State $state
     * @param SDKClient $lucretiaClient
     * @param ProductRepositoryFactory $productRepositoryFactory
     * @param ProductHelper $productHelper
     * @param ProductAttributeMediaGalleryManagementInterface $productAttributeMediaGalleryManagement
     * @param EntryFactory $productGalleryEntryFactory
     * @param ImageContentFactory $imageContentFactory
     */
    public function __construct(
        State $state,
        SDKClient $lucretiaClient,
        ProductRepositoryFactory $productRepositoryFactory,
        ProductHelper $productHelper,
        ProductAttributeMediaGalleryManagementInterface $productAttributeMediaGalleryManagement,
        EntryFactory $productGalleryEntryFactory,
        ImageContentFactory $imageContentFactory
    ) {
        parent::__construct($state, $lucretiaClient);
        $this->_productRepository = $productRepositoryFactory->create();
        $this->_productHelper = $productHelper;
        $this->_productAttributeMediaGalleryManagement = $productAttributeMediaGalleryManagement;
        $this->_productGalleryEntryFactory = $productGalleryEntryFactory;
        $this->_imageContentFactory = $imageContentFactory;
    }

    /**
     * {@inheritdoc}
     * @void
     */
    protected function configure()//: void
    {
        $this->setName('lucretia:product:images');
        $this->setDescription('Fetches all product images from Lucretia and append said images to products in Magento');
        parent::configure();
    }

    /**
     * @param ConsoleInputInterface $input
     * @param ConsoleOutputInterface $output
     * @return int|null
     */
    protected function execute(ConsoleInputInterface $input, ConsoleOutputInterface $output): int
    {
        $output->writeln('Started at ' . date("H:i:s"));
        $numberOfRecordsProcessed = 0;

        try {
            foreach ($this->_lucretiaClient->getProductImages() as $lucretiaProductImagesRecord) {
                try {
                    // Process counter
                    $numberOfRecordsProcessed++;

                    // Attempt to fetch the product
                    try {
                        $product = $this->_productRepository->get($lucretiaProductImagesRecord->Itemcode);
                    } catch (\Exception $e) {
                        continue;
                    }

                    // Remove all existing images
                    if (($productMediaGalleryEntries = $product->getMediaGalleryEntries()) !== NULL) {
                        foreach ($productMediaGalleryEntries as $productAttributeMediaGalleryEntry) {
                            $this->_productAttributeMediaGalleryManagement->remove($product->getSku(), $productAttributeMediaGalleryEntry->getId());
                        }
                    }

                    // Add new media entries
                    for ($i = 0; $i <= 6; $i++) {
                        if (isset($lucretiaProductImagesRecord->{"Afbeelding" . $i}) && $this->_productHelper->isBase64($lucretiaProductImagesRecord->{"Afbeelding" . $i})) {
                            try {
                                // Create image content
                                $imageInfo = getimagesizefromstring(base64_decode($lucretiaProductImagesRecord->{"Afbeelding" . $i}));
                                $imageContent = $this->_imageContentFactory->create();
                                $imageContent->setBase64EncodedData($lucretiaProductImagesRecord->{"Afbeelding" . $i});
                                $imageContent->setType($imageInfo['mime']);
                                $imageContent->setName($product->getSku() . '-' . uniqid() . $this->_productHelper->getExtensionForMime($imageInfo['mime']));

                                // Create entry
                                $productAttributeMediaGalleryEntry = $this->_productGalleryEntryFactory->create();
                                $productAttributeMediaGalleryEntry->setMediaType('image');
                                $productAttributeMediaGalleryEntry->setLabel($product->getSku() . '-' . uniqid() . $this->_productHelper->getExtensionForMime($imageInfo['mime']));
                                $productAttributeMediaGalleryEntry->setPosition(($i + 1));
                                $productAttributeMediaGalleryEntry->setDisabled(false);
                                $productAttributeMediaGalleryEntry->setContent($imageContent);
                                $productAttributeMediaGalleryEntry->setTypes($i == 0 ? (['image', 'thumbnail', 'small_image']) : []);

                                // Save entry
                                $this->_productAttributeMediaGalleryManagement->create($product->getSku(), $productAttributeMediaGalleryEntry);
                            } catch (\Exception $e) {
                                $output->writeln('Failed to generate image on image entry ' . $i);
                                $output->writeln('Generation exception - ' . $e->getMessage());
                            }
                        }
                    }
                } catch (\Exception $e) {
                    $output->writeln('Caught generic error during product image handling. Continuing..');
                    $output->writeln('Generic exception - ' . $e->getMessage());
                }
            }
        } catch (\Exception $e) {
            $output->writeln('Caught generic error! Process halted.');
            $output->writeln('Generic exception - ' . $e->getMessage());
        }

        $output->writeln('Process finished at ' . date("H:i:s"));
        $output->writeln('Processed ' . number_format($numberOfRecordsProcessed) . ' records.');

        return 0;
    }
}
