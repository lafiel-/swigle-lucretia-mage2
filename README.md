# README #

## What is this repository for? ##

The Swigle-Lucretia-Mage2 package contains a the Lucretia intergration module for Magento2.

# DIRECTORY STRUCTURE

      src/                contains classes
      vendor/             contains dependent 3rd-party packages


# REQUIREMENTS

These are the  minimum requirement required for this library:
*  PHP 7.0.0
*  Magento Framework 101.0.0 
*  Magento store module 100.2

# INSTALLATION

### Install via Composer

You can install this package by adding the following to your composer.json:
NOTE: chances are that you already have a `repositories` section, in this case you can add the repositories directly

~~~
    "repositories": {
        "swigle-mage2": {
           "type": "vcs",
           "url": "https://bitbucket.org/swigle/swigle-lucretia-mage2.git"
        },
        "swigle-sdk": {
           "type": "vcs",
          "url": "https://bitbucket.org/swigle/swigle-lucretia-sdk.git"
        }
    }
~~~

After this you can then install this package using the following command:

~~~
composer require swigle/lucretia-mage2
~~~

